
import HomeScreen from '../screens/HomeScreen';
import SupportScreen from '../screens/SupportScreen';
import CardsScreen from '../screens/CardsScreen';
import ScanQrScreen from '../screens/ScanQrScreen';
import TransferScreen from '../screens/TransferScreen';
import PaymentScreen from '../screens/PaymentScreen';
import AccountScreen from '../screens/AccountScreen';
import HistoryScreen from '../screens/HistoryScreen';
import SettingScreen from '../screens/SettingScreen';

export {HomeScreen, SupportScreen, CardsScreen, ScanQrScreen, TransferScreen, PaymentScreen, AccountScreen, HistoryScreen, SettingScreen}