const tintColorLight = '#28d778';
const tintColorLight2 = '#18f768';
const tint2 = '#1ca4b4';
const tint3 = '#9cb4ff';
const tint4 = '#f4949c';
const tint5 = '#dcd4d4';
const tintColorDark = '#fff';

export default {
  light: {
    text: '#000',
    textLight: '#252525',
    boxShadows: '#252525',
    background: '#fff',
    background2: '#fff',
    background3: '#f3f4ff',
    background4: '#faffff',
    background5: '#f3fff4',
    backgroundOverlay: '#f3fff4',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    accent : tintColorLight,
    accent2 : tint2,
    accent3 : tint3,
    accent4 : tint4,
    accent5 : tint5,
    divider : '#adadad'
  },
  dark: {
    text: '#fff',
    textLight: '#dcdcdc',
    boxShadows: '#000',
    background: '#000',
    background2: '#252525',
    background3: '#121212',
    background4: '#353535',
    background5: '#353535',
    backgroundOverlay: '#252525',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    accent : tintColorLight,
    accent2 : tint2,
    accent3 : tint3,
    accent4 : tint4,
    accent5 : tint5,
    divider : '#454545'
  },
};
